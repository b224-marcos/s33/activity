
// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response) => response.json())
// .then((json) => console.table(json));

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then(json => {
    const titleArray = json.map((elem) => {
     return elem.title 
   })
   console.log(titleArray)
})



//6.

/*fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))
console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)*/

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {
console.log(json)
console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)
})






//7.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		"Content-type" : "application/json"
	},

	body: JSON.stringify({
		completed: false,
		title: "Created to do list item",
		userId: 1
	})


})
.then((response) => response.json())
.then((json) => console.log(json));


//9.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure",
        id : 1,
        status: "Pending",
        title: "Updated to do list item",
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

//10.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        
        dateCompleted: "07/09/22",
       
        status: "Complete",
        
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

//13.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: "DELETE"
})
